# Spring Cloud微服务电商项目

#### 介绍


#### 软件架构
A. 项目采用SpringBoot2.x+SpringCloud2.x构建微服务电商项目

1. 使用SpringCloudEureka作为注册中心，实现服务治理
1. 使用Zuul网关框架管理服务请求入口
1. 使用Ribbon实现本地负载均衡器和FeginHttp客户端调用工具
1. 使用Hystrix服务保护框架(服务降级、隔离、熔断、限流)
1. 使用消息总线Stream RabbitMQ和Kafka
1. 微服务API接口安全控制与单点登陆系统CAS+JWT+Oauth2.0



B. 分布式基础设施环境构建

1. 分布式任务调度平台XXL-Job
1. 分布式日志采集系统ELK +kafka
1. 分布式事务解决方案LCN 
1. 分布式锁解决方案Zookeeper、Redis
1. 分布式配置中心携程阿波罗 
1. 高并发分布式全局ID生成雪花算法
1. 分布式Session框架Spring-Session
1. 分布式服务追踪与调用链ZipKin  
1. 



C.项目运营与部署环境

1. 分布式设施环境，统一采用docker安装
1. 使用jenkins+docker+k8s实现自动部署 
1. 微服务API管理ApiSwagger
1. 使用GitLab代码管理 
1. 统一采用第三方云数据库
1. 使用七牛云服务器对静态资源实现加速
1. 构建企业级Maven私服


#### 学习系统要求


1. 建议电脑配置CPU在I5、32GB内存及以上
1. JDK版本:JDK1.8
1. Maven 统一管理Jar
1. 统一采用Docker安装软件
1. 编码统一采用为UTF-8
1. 开发工具IDE或者Eclipse


#### 项目构建

1.meite-shop-parent-----公共Pranet接口 pom 
- meite-shop-basics----分布式基础设施 pom 
- - meite-shop-basics-springcloud-eureka—注册中心 8080
- - meite-shop-basics-apollo-config-server—阿波罗分布式配置中心
- - meite-shop-basics-springcloud-zuul—统一请求入口 80
- - meite-shop-basics-xuxueli-xxljob—分布式任务调度平台
- - meite-shop-basics-codingapi-lcn—分布式事务解决框架
- - meite-shop-basics-codingapi- ZipKin  —分布式调用链系统


2.meite-shop-service-api提供公共接口 pom
- - meite-shop-service-api-weixin 微信服务接口
- - meite-shop-service-api-member会员服务接口
- - meite-shop-service-api-sso  sso服务接口
- - meite-shop-service-api-item商品服务接口
- - meite-shop-service-api-search 搜索服务接口
- - meite-shop-service-api-pay聚合支付平台
- - meite-shop-service-api-order订单服务接口
- - meite-shop-service-api-spike 秒杀服务接口
服务接口中包含内存内容: 实体类层、接口层 

3.meite-shop-service-impl公共接口的实现 pom 
- - meite-shop-service-weixin 微信服务接口实现
- - meite-shop-service-member会员服务接口实现
- - meite-shop-service-api-sso  sso服务接口实现
- - meite-shop-service-tem商品服务接口实现
- - meite-shop-service-search 搜索服务接口实现
- - meite-shop-service-pay聚合支付平台接口实现
- - meite-shop-service-order订单服务接口实现
- - meite-shop-service-api-spike 秒杀服务接口

4.meite-shop-common 工具类 pom 
- - meite-shop-common-core—核心工具类

5.meite-shop-portal 门户平台 pom
- - meite-shop-portal-web 门户网站 
- - meite-shop-portal-sso 单点登陆系统 
- - meite-shop-portal-search 搜索系统
- - meite-shop-portal-spike 秒杀系统
- - meite-shop-portal-cms 系统 

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request